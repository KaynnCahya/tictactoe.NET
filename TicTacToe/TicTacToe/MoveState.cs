﻿namespace TicTacToe {
    [System.Serializable]
    public enum MoveState {
        ERROR_OCCUPIED,
        WINNING_MOVE,
        CONTINUE
    }
}
