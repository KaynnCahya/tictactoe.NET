﻿namespace TicTacToe {
    [System.Serializable]
    public struct Position {

        public int X {
            get; private set;
        }

        public int Y {
            get; private set;
        }

        internal Position(int _x, int _y) {
            X = _x;
            Y = _y;
        }
    }
}
