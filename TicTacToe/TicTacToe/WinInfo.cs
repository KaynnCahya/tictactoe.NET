﻿namespace TicTacToe {
    public class WinInfo {

        public byte WinningPlayerNumber {
            get; private set;
        }

        private Position[] winningTiles;

        internal WinInfo(Position[] _winningTiles, Marking _winningMarker) {
            winningTiles = _winningTiles;

            if (_winningMarker == Marking.O) {
                WinningPlayerNumber = 2;
            } else {
                WinningPlayerNumber = 1;
            }
        }

        /// <summary>
        /// Contains the 3 positions that caused a win.
        /// </summary>
        /// <returns></returns>
        public Position[] GetWinningPositions() {
            return winningTiles;
        }
    }
}
