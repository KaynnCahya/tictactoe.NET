﻿

namespace TicTacToe {
    [System.Serializable]
    public enum Marking {
        UNOCCUPIED,
        X = 1,
        O = 2
    }
}
