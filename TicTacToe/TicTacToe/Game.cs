﻿using System;
using System.Collections.Generic;

namespace TicTacToe {
    public class Game {

        //The game's board itself.
        private Marking[,] board = new Marking[3, 3];

        /// <summary>
        /// Which player's turn it is
        /// X is Player 1.
        /// O is Player 2.
        /// </summary>
        public bool IsPlayer1Turn {
            get; private set;
        }

        public bool GameWon {
            get; private set;
        }

        public WinInfo WinInfo {
            get; private set;
        }

        public Game() {
            ResetGame();
        }

        public Marking[,] GetBoard() {
            return board;
        }

        /// <summary>
        /// Resets the current game.
        /// </summary>
        public void ResetGame() {
            IsPlayer1Turn = true;
            GameWon = false;
            //Resets the game board.
            for (int i = 0; i < 3; ++i) {
                for (int n = 0; n < 3; ++n) {
                    board[i, n] = Marking.UNOCCUPIED;
                }
            }
            WinInfo = null;
        }

        private MoveState UpdateBoard(byte x, byte y) {

            Marking marking;
            if (IsPlayer1Turn) {
                marking = Marking.X;
            } else {
                marking = Marking.O;
            }

            //If the marked tile was not occupied.
            if (board[x, y] == 0) {
                board[x, y] = marking;

                if (WonByRow()) {
                    return MoveState.WINNING_MOVE;
                }

                if (WonByColumn()) {
                    return MoveState.WINNING_MOVE;
                }

                if (WonByDiagonal()) {
                    return MoveState.WINNING_MOVE;
                }

                return MoveState.CONTINUE;
            } else {
                return MoveState.ERROR_OCCUPIED;
            }

            #region Local_Function

            bool WonByRow() {
                List<Position> winningPos = new List<Position>();

                int counter = 0;
                for (int row = 0; row < 3; ++row) {
                    for (int column = 0; column < 3; ++column) {
                        if (board[row, column] == marking) {
                            ++counter;
                            winningPos.Add(new Position(row, column));
                        }
                    }
                    if (counter == 3) {
                        WinInfo = new WinInfo(winningPos.ToArray(), marking);
                        return true;
                    }
                    counter = 0;
                    winningPos.Clear();
                }

                return false;
            }

            bool WonByColumn() {
                int counter = 0;
                List<Position> winningPos = new List<Position>();
                for (int column = 0; column < 3; ++column) {
                    for (int row = 0; row < 3; ++row) {
                        if (board[row, column] == marking) {
                            ++counter;
                            winningPos.Add(new Position(row, column));
                        }
                    }
                    if (counter == 3) {
                        WinInfo = new WinInfo(winningPos.ToArray(), marking);
                        return true;
                    }
                    counter = 0;
                }

                return false;
            }

            bool WonByDiagonal() {

                int leftRight = (int)board[0, 0] + (int)board[1, 1] + (int)board[2, 2];
                if (leftRight == (int)marking * 3) {
                    var winningPos = new Position[] {
                        new Position(0, 0),
                        new Position(1, 1),
                        new Position(2, 2)
                    };

                    WinInfo = new WinInfo(winningPos, marking);
                    return true;
                }

                int rightLeft = (int)board[0, 2] + (int)board[1, 1] + (int)board[2, 0];
                if (rightLeft == (int)marking * 3) {
                    var winningPos = new Position[] {
                        new Position(0, 2),
                        new Position(1, 1),
                        new Position(2, 0)
                    };

                    WinInfo = new WinInfo(winningPos, marking);
                    return true;
                }

                return false;
            }

            #endregion
        }

        public MoveState NewMove(byte row, byte column) {
            if (GameWon) {
                ResetGame();
            }

            if (row > 2 || column > 2) {
                throw new InvalidOperationException("New move is out of board position.");
            }

            return DoMove(row, column);
        }

        public MoveState NewMove(byte tileNumber) {
            if (GameWon) {
                ResetGame();
            }

            if (tileNumber == 0 || tileNumber > 9) {
                throw new InvalidOperationException("New move is out of board position.");
            }

            byte row = 0, column = 0;

            for (byte i = 1, x = 0, y = 0; i < 10; ++i) {
                if (i == tileNumber) {
                    row = x;
                    column = y;
                    break;
                }
                ++y;
                if (y == 3) {
                    ++x;
                    y = 0;
                }
            }

            /*
             Tile number is visualized as this way:
             1  2   3
             4  5   6
             7  8   9
             */

            return DoMove(row, column);
        }

        private MoveState DoMove(byte row, byte column) {
            MoveState state = UpdateBoard(row, column);

            if (state == MoveState.WINNING_MOVE) {
                GameWon = true;
            } else if (state == MoveState.CONTINUE) {
                SwapPlayerTurn();
            }

            return state;

            #region Local_Function

            void SwapPlayerTurn() {
                if (IsPlayer1Turn) {
                    IsPlayer1Turn = false;
                } else {
                    IsPlayer1Turn = true;
                }
            }

            #endregion
        }
    }
}
