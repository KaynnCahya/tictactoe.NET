# TicTacToe.NET

Nuget library for handling the backend of a TicTacToe game.<br>
You may download this project from Nuget [here](https://www.nuget.org/packages/TicTacToe.NET/1.0.0).

### How To

Reference to the namespace `TicTacToe`.
Create a new game by instantiating a `Game` object.

The game will be automatically initalized upon creation, just call `Game.NewMove(x, y)` to plot a marking for the current player.<br>
The function will return a `MoveState`, indicating if the move will cause a win, or the game will continue.

Use `Game.IsPlayer1Turn` to determine which player's turn it is.

If you need information on how a game was won, `Game.WinInfo` provides it.
